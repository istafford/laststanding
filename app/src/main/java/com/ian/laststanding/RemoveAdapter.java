package com.ian.laststanding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class RemoveAdapter extends ArrayAdapter<PlayerClass> implements View.OnClickListener {
    private Context context;
    private List<PlayerClass> playerDetais;
    private String playerName;
    private String uID;
    private String tournament;

//constructor, call on creation
public RemoveAdapter(Context context, int resource, ArrayList<PlayerClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.playerDetais = objects;
        }

//called when rendering the list
public View getView(int position, View convertView, ViewGroup parent) {

        //get the property we are displaying
        PlayerClass players = playerDetais.get(position);

        playerName = players.getPlayerName();
        uID = players.getuID();
        tournament = players.getTournament();

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.remove_list_layout, null);

        TextView player = (TextView) view.findViewById(R.id.player);
        TextView removeBtn = (Button) view.findViewById(R.id.removeBtn);

        //set tournament and admin
        player.setText(players.getPlayerName());
        removeBtn.setOnClickListener(this);

        return view;
        }

public void remove(){
    AlertDialog.Builder builder;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
    } else {
        builder = new AlertDialog.Builder(context);
    }
    builder.setTitle("Remove Player!")
            .setMessage("Are you sure you want to remove this player from the tournament?")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference ref = database.getReference("Tournaments").child(tournament).child("Players").child(uID);
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            dataSnapshot.getRef().removeValue();
                            DatabaseReference ref = database.getReference("Users").child(uID).child("Tournaments").child(tournament);
                            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    dataSnapshot.getRef().removeValue();
                                    System.out.println();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            })
            .setIcon(android.R.drawable.ic_delete)
            .show();
        }

        @Override
        public void onClick(View v) {remove();}
}