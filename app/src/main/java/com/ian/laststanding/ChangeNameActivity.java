package com.ian.laststanding;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChangeNameActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText newFName;
    private EditText newLName;
    private EditText pswd;
    private Button saveNameBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);

        newFName = (EditText) findViewById(R.id.newFName);
        newLName = (EditText) findViewById(R.id.newLName);
        pswd = (EditText) findViewById(R.id.emailPswd);
        saveNameBtn = (Button) findViewById(R.id.saveEmailBtn);


        saveNameBtn.setOnClickListener(this);
    }

    public void updateName(){

    }

    public void hideKeyboard() {
        try  {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String fName = newFName.getText().toString().trim();
        final String lName = newLName.getText().toString().trim();
        final String password = pswd.getText().toString().trim();

        if (TextUtils.isEmpty(fName)) {
            Toast.makeText(getApplicationContext(), "Enter first name!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(lName)) {
            Toast.makeText(getApplicationContext(), "Enter last name!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            return;
        }
        final String name = fName + " " + lName;

        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), password);

        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    String uid = user.getUid();
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference userInfoRef = database.getReference("Users");
                    userInfoRef.child(uid).child("First Name").setValue(fName);
                    userInfoRef.child(uid).child("Last Name").setValue(lName);
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
                    user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Name was successfully changed!", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ChangeNameActivity.this, ProfileActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), "Failed to change name!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(ChangeNameActivity.this, "An error occurred.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
