package com.ian.laststanding;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class TournamentPlayerActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView picture;
    private StorageReference mStorage;
    private ListView dListView;
    private TextView dCount;
    private int playerCount;
    private String mUserID;
    private String tournamentName;
    private Button levaeBtn;
    private ArrayList<String> players = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_player);

        tournamentName = getIntent().getExtras().getString("tournament_name");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUserID = user.getUid();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tournaments").child(tournamentName).child("Players");

        levaeBtn = (Button) findViewById(R.id.leaveBtn);
        levaeBtn.setOnClickListener(this);

        // List Views
        dListView = (ListView) findViewById(R.id.playerList);
        dCount = (TextView) findViewById(R.id.count);
        final ArrayAdapter<String> playerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, players);
        dListView.setAdapter(playerAdapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                playerAdapter.clear();
                playerAdapter.notifyDataSetChanged();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    playerCount++;
                    DatabaseReference ref = database.getReference("Users").child(snap.getKey());
                    ref.addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String playerName = null;
                            playerName = dataSnapshot.child("First Name").getValue().toString() + " " + dataSnapshot.child("Last Name").getValue().toString();
                            playerAdapter.add(playerName);
                            playerAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                dCount.setText(""+playerCount);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });
    }

    public void logOut (View v) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(TournamentPlayerActivity.this, SplashScreen.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Leave Tournament!")
                .setMessage("Are you sure you want to leave the tournament?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference ref = database.getReference("Tournaments").child(tournamentName).child("Players").child(mUserID);
                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                dataSnapshot.getRef().removeValue();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        DatabaseReference ref1 = database.getReference("Users").child(mUserID).child("Tournaments").child(tournamentName);
                        ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                dataSnapshot.getRef().removeValue();
                                System.out.println();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void open(){
        Intent intent = new Intent(this, TournamentActivity.class);
        startActivity(intent);
    }
}
