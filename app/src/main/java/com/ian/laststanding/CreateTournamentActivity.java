package com.ian.laststanding;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CreateTournamentActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextTName;
    private Button createBtn;
    private TextView test;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String uID;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tournament);

        editTextTName = (EditText)findViewById(R.id.newTournamentName);
        createBtn = (Button)findViewById(R.id.createTournBtn);

        uID = user.getUid();


        progressDialog = new ProgressDialog(this);

        //attaching click listener
        createBtn.setOnClickListener(this);
    }

    public void createTournament(){
        hideKeyboard();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final String tname = editTextTName.getText().toString().toUpperCase().trim();

        if (TextUtils.isEmpty(tname)) {
            Toast.makeText(getApplicationContext(), "Enter Tournament Name!", Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            final DatabaseReference tournInfoRef = database.getReference("Tournaments").child(tname);

            tournInfoRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Toast.makeText(getApplicationContext(), "Tournament name is already in use!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                    else if(!dataSnapshot.exists()) {
                        String tName = editTextTName.getText().toString().toUpperCase().trim();
                        String s = user.getDisplayName();
                        DatabaseReference newTournInfo = database.getReference("Tournaments");
                        newTournInfo.child(tName).child("Admin").child("Admin ID").setValue(uID);
                        newTournInfo.child(tName).child("Admin").child("Admin Name").setValue(user.getDisplayName());
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Arsenal").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Bournemouth").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Burnley").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Chelsea").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Crystal Palace").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Everton").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Hull City").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Leicester").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Liverpool").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Man City").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Man Utd").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Middlesbrough").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Southampton").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Stoke").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Sunderland").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Swansea").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Tottenham").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Watford").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("West Brom").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("West Ham").setValue(true);
                        newTournInfo.child(tName).child("Players").child(uID).child("Team").setValue(false);
                        newTournInfo.child(tName).child("Players").child(uID).child("Standing").setValue(true);
                        DatabaseReference newUserInfo = database.getReference("Users");
                        newUserInfo.child(uID).child("Tournaments").child(tName).child("Admin").setValue(user.getDisplayName());
                        Toast.makeText(getApplicationContext(), "Tournament Created", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        open();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void open() {
        Intent intent = new Intent(this, TournamentActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        progressDialog.setMessage("Creating Tournament Please Wait...");
        progressDialog.show();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Create Tournament")
                .setMessage("Are you sure you want to create this tournament?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        createTournament();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_input_add)
                .show();
    }


    public void hideKeyboard() {
        try  {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

}
