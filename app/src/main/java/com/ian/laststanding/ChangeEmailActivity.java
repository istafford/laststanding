package com.ian.laststanding;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChangeEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText newEmail;
    private EditText pswd;
    private Button saveEmailBtn;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);

        newEmail = (EditText) findViewById(R.id.newEmail);
        pswd = (EditText) findViewById(R.id.emailPswd);
        saveEmailBtn = (Button) findViewById(R.id.saveEmailBtn);

        progressDialog = new ProgressDialog(this);
        saveEmailBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        progressDialog.setMessage("Updating Email Please Wait...");
        progressDialog.show();
        updateEmail();
    }

    public void hideKeyboard() {
        try  {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public void updateEmail(){
        hideKeyboard();
        final String email = newEmail.getText().toString().trim();
        String password  = pswd.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter email!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            return;
        }

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), password);

        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    String uid = user.getUid();
                    user.updateEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.dismiss();
                                        Toast.makeText(ChangeEmailActivity.this, "Email was changed successfully!.", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(ChangeEmailActivity.this, ProfileActivity.class));
                                    }
                                }
                            });
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ChangeEmailActivity.this, "Error, email was not changed!.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
