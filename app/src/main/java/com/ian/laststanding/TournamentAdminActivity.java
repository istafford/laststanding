package com.ian.laststanding;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TournamentAdminActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView mListView;
    private Button deleteBtn;
    private Button restartBtn;
    private Button resetBtn;
    private ArrayList<PlayerClass> playerDetails = new ArrayList<>();
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    String title;
    String mUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_admin);

        deleteBtn = (Button) findViewById(R.id.deleteTournamentBtn);
        deleteBtn.setOnClickListener(this);

        resetBtn = (Button)findViewById(R.id.resetTournamentBtn);
        resetBtn.setOnClickListener(this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUserID = user.getUid();
        title= getIntent().getExtras().getString("tournament_name");

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tournaments").child(title).child("Players");

        mListView = (ListView) findViewById(R.id.removeList);
        final ArrayAdapter<PlayerClass> playerAdapter = new RemoveAdapter(TournamentAdminActivity.this, 0, playerDetails);
        mListView.setAdapter(playerAdapter);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                playerAdapter.clear();
                playerAdapter.notifyDataSetChanged();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    final String id = snap.getKey();
                    if (!mUserID.equals(snap.getKey())) {
                        DatabaseReference ref = database.getReference("Users").child(snap.getKey());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String playerName = dataSnapshot.child("First Name").getValue().toString() + " " + dataSnapshot.child("Last Name").getValue().toString();
                                playerAdapter.add(new PlayerClass(playerName, id, title));
                                playerAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        playerAdapter.notifyDataSetChanged();
    }

    public void deleteTournament(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Delete Tournament")
                .setMessage("Are you sure you want to delete this tournament?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteFromUser();
                        deleteFromTournaments();
                        home();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

    public void deleteFromTournaments(){
        DatabaseReference ref = database.getReference("Tournaments").child(title);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void deleteFromUser(){
        DatabaseReference ref = database.getReference("Users");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    System.out.println("***************************DELETE KEY2*************************************");
                    System.out.println(snap.getKey());
                    DatabaseReference ref = database.getReference("Users").child(snap.getKey()).child("Tournaments").child(title);
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                if(snap.exists()) {
                                    System.out.println("***************************Tournament*******************************");
                                    snap.getRef().removeValue();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void restart(){
        final DatabaseReference newTournInfo = database.getReference("Tournaments").child(title).child("Players");
        newTournInfo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    System.out.println("***************************PLAYERS*************************************");
                    System.out.println(snap.getKey());
                    final String UserID = snap.getKey();
                    final DatabaseReference newPlayerInfo = database.getReference("Tournaments");
                    newPlayerInfo.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Arsenal").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Bournemouth").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Burnley").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Chelsea").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Crystal Palace").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Everton").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Hull City").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Leicester").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Liverpool").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Man City").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Man Utd").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Middlesbrough").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Southampton").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Stoke").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Sunderland").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Swansea").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Tottenham").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("Watford").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("West Brom").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Available Teams").child("West Ham").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Standing").setValue(true);
                            newPlayerInfo.child(title).child("Players").child(UserID).child("Team").setValue(false);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*
        */
    }

    public void restartTournament(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Restart Tournament")
                .setMessage("Are you sure you want to restart this tournament?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        restart();
                        home();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onClick(View v) {
        if(v == deleteBtn) {
            deleteTournament();
        }
        else if(v == resetBtn){
            restartTournament();
        }
    }

    public void home(){
        Intent intent = new Intent(this, TournamentActivity.class);
        startActivity(intent);
    }


}
