package com.ian.laststanding;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

import static android.R.id.list;

public class TournamentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ArrayList<TournamentClass> tournamentInfo = new ArrayList<>();

    private String mUser_name;
    private TextView mUser_email;
    private String mUser_ID;

    private ListView mListView;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1, floatingActionButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament);
        //  tournamentList = (ListView) findViewById(R.id.tournamentList);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUser_ID = user.getUid();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Users").child(mUser_ID).child("Tournaments");

        // List Views
        mListView = (ListView) findViewById(R.id.tournamentList);
        final ArrayAdapter<TournamentClass> tournamentAdapter = new TournamentAdapter(TournamentActivity.this, 0, tournamentInfo);
        mListView.setAdapter(tournamentAdapter);


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("*************************************************************************************************************************************************************************");
                System.out.println(dataSnapshot.getValue());
                tournamentAdapter.clear();
                tournamentAdapter.notifyDataSetChanged();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    System.out.println("*************************************************************************************************************************************************************************");
                    System.out.println(snap.getValue());
                    String tournamentName = snap.getKey().toString();
                    String adminName = snap.child("Admin").getValue(String.class);
                    tournamentAdapter.add(new TournamentClass(tournamentName,adminName));
                    tournamentAdapter.notifyDataSetChanged();
                }
                System.out.println("*************************************************************************************************************************************************************************");
                System.out.println(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
                {
                    // Get Person "behind" the clicked item
                    TournamentClass t = (TournamentClass) mListView.getItemAtPosition(position);

                    // Log the fields to check if we got the info we want
                    Log.i("SomeTag", "Persons name: " + t.tName);
                    Log.i("SomeTag", "Persons id : " + t.aName);

                    // Do something with the data. For example, passing them to a new Activity

                    Intent i = new Intent(TournamentActivity.this, TournamentDetailsActivity.class);
                    i.putExtra("tournament_name", t.tName);
                    i.putExtra("admin_name", t.aName);

                    TournamentActivity.this.startActivity(i);
                }
                /*String cities = String.valueOf(parent.getItemAtPosition(position));
                Class<?> selectedFromList =(mListView.getItemAtPosition(position).getClass());
                Intent intent = new Intent(view.getContext(), TournamentDetailsActivity.class);
                startActivityForResult(intent, 0);
                intent.putExtra("Title", cities);
                startActivity(intent);*/
            });


        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TournamentActivity.this, CreateTournamentActivity.class);
                startActivity(intent);
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TournamentActivity.this, JoinTournamentActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tournament, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tournament) {
            onBackPressed();
        } else if (id == R.id.nav_fixtures) {
            Intent intent = new Intent(this, FixturesAndResultsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_how_to_play) {
            Intent intent = new Intent(this, HowToPlayActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_help) {
            Intent intent = new Intent(this, UpdateActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logOut (View v) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(TournamentActivity.this, SplashScreen.class);
        startActivity(intent);
    }

    public void createTournament(View view) {
        Intent intent = new Intent(this, CreateTournamentActivity.class);
        startActivity(intent);
    }


}
