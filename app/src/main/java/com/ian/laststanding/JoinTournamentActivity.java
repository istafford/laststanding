package com.ian.laststanding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.R.id.list;

public class JoinTournamentActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<TournamentClass> tournamentInfo = new ArrayList<>();

    private EditText searchValue;
    private String mUser_ID;
    private Button searchBtn;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_tournament);

        searchBtn = (Button) findViewById(R.id.searchBtn);
        mListView = (ListView) findViewById(R.id.resultVew);
        searchValue = (EditText) findViewById(R.id.searchValue);

        searchBtn.setOnClickListener(this);
    }

    public void search(){
        final String name = searchValue.getText().toString().toUpperCase().trim();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUser_ID = user.getUid();

        System.out.println("********************************************SEARCH***************************************************************");
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("Tournaments").child(name);

        // List Views
        final ArrayAdapter<TournamentClass> joinAdapter = new JoinAdapter(JoinTournamentActivity.this, 0, tournamentInfo);
        mListView.setAdapter(joinAdapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    hideKeyboard();
                    System.out.println("********************************************SEARCH***************************************************************");
                    System.out.println(dataSnapshot.getValue());
                    joinAdapter.clear();
                    Log.e("ERROR","A");
                    joinAdapter.notifyDataSetChanged();
                    Log.e("ERROR","B");
                    String adminName = dataSnapshot.child("Admin").child("Admin Name").getValue(String.class);
                    System.out.println("********************************************SEARCHADMIN***************************************************************");
                    System.out.println(adminName);
                    Log.e("ERROR","E");
                    joinAdapter.add(new TournamentClass(name,adminName));
                    Log.e("ERROR","F");
                    joinAdapter.notifyDataSetChanged();
                    Log.e("ERROR","G");/**/
                }
                else{
                    Toast.makeText(getApplicationContext(), "Tournament does not exist!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

    }

    @Override
    public void onClick(View v) {
        search();
    }

    public void hideKeyboard() {
        try  {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
}
