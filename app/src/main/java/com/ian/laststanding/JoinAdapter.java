package com.ian.laststanding;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by istaf on 24/05/2017.
 */

public class JoinAdapter extends ArrayAdapter<TournamentClass> implements View.OnClickListener {
    private Context context;
    private List<TournamentClass> tournamentDetails;
    private String tournamentName;
    private String adminName;

    //constructor, call on creation
    public JoinAdapter(Context context, int resource, ArrayList<TournamentClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.tournamentDetails = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {

        //get the property we are displaying
        TournamentClass tournaments = tournamentDetails.get(position);
        tournamentName = tournaments.tName;
        adminName = tournaments.aName;

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.join_list_layout, null);

        TextView tournamentName = (TextView) view.findViewById(R.id.tournament_name);
        TextView adminName = (TextView) view.findViewById(R.id.admin_name);
        Button joinBtn = (Button) view.findViewById(R.id.joinBtn);

        //set tournament and admin
        tournamentName.setText(tournaments.tName);
        adminName.setText(tournaments.aName);
        joinBtn.setOnClickListener(this);

        return view;
    }

    public void join(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uID = user.getUid();
        String uName = user.getDisplayName();
        System.out.println(uID);
        System.out.println(uName);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference newTournInfo = database.getReference("Tournaments");
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Standing").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Arsenal").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Bournemouth").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Burnley").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Chelsea").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Crystal Palace").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Everton").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Hull City").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Leicester").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Liverpool").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Man City").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Man Utd").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Middlesbrough").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Southampton").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Stoke").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Sunderland").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Swansea").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Tottenham").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("Watford").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("West Brom").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Available Teams").child("West Ham").setValue(true);
        newTournInfo.child(tournamentName).child("Players").child(uID).child("Team").setValue(false);
        newTournInfo.child(tournamentName).child("Winner").setValue(false);/* */
        DatabaseReference newUserInfo = database.getReference("Users");
        System.out.println(uID);
        newUserInfo.child(uID).child("Tournaments").child(tournamentName).child("Admin").setValue(adminName);
        Toast.makeText(context, "You Joined the " + tournamentName + " tournament", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle("Join Tournament")
                .setMessage("Are you sure you want to join this tournament?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        join();
                        open();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_input_add)
                .show();
    }

    public void open(){
        Intent intent = new Intent(context, TournamentActivity.class);
        context.startActivity(intent);
    }
}
