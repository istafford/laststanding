package com.ian.laststanding;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FixturesAdapter extends ArrayAdapter<FixturesClass> {
    private Context context;
    private List<FixturesClass> teamDetails;

    //constructor, call on creation
    public FixturesAdapter(Context context, int resource, ArrayList<FixturesClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.teamDetails = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {
        //get the property we are displaying
        FixturesClass teams = teamDetails.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fixture_list_layout, null);


        TextView homeName = (TextView) view.findViewById(R.id.homeTeam);
        ImageView homeCrest = (ImageView) view.findViewById(R.id.homeCrest);
        TextView homeScore = (TextView) view.findViewById(R.id.homeScore);
        TextView awayName = (TextView) view.findViewById(R.id.awayTeam);
        ImageView awayCrest = (ImageView) view.findViewById(R.id.awayCrest);
        TextView awayScore = (TextView) view.findViewById(R.id.awayScore);

        homeName.setText(teams.homeTeam);
        homeScore.setText(teams.homeGoals);
        awayName.setText(teams.awayTeam);
        awayScore.setText(teams.awayGoals);

        //get the image associated with this property
        int crest = context.getResources().getIdentifier(teams.getHomeCrest(), "drawable", context.getPackageName());
        homeCrest.setImageResource(crest);

        int crest2 = context.getResources().getIdentifier(teams.getAwayCrest(), "drawable", context.getPackageName());
        awayCrest.setImageResource(crest2);

        return view;
    }
}
