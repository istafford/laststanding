package com.ian.laststanding;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StandingAdapter extends ArrayAdapter<StandingClass> {
    private Context context;
    private List<StandingClass> standingDetails;

    //constructor, call on creation
    public StandingAdapter(Context context, int resource, ArrayList<StandingClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.standingDetails = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {

        //get the property we are displaying
        StandingClass tournaments = standingDetails.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.standing_list_layout, null);

        TextView playerName = (TextView) view.findViewById(R.id.pName);
        TextView standingStatus = (TextView) view.findViewById(R.id.standing);

        //set tournament and admin
        playerName.setText(tournaments.playerName);
        standingStatus.setText(tournaments.standing);

        if(tournaments.getStanding().equals("true")){
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.in));
            standingStatus.setText("Standing");
        }
        else if(tournaments.getStanding().equals("Winner")){
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.gold));
            standingStatus.setText("WINNER");
            standingStatus.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            playerName.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        }
        else {
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.out));
            standingStatus.setText("Out");
        }

        return view;
    }
}
