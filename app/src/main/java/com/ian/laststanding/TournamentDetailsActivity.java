package com.ian.laststanding;

import android.app.TabActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;


public class TournamentDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tName;
    private ListView mListView;
    private ArrayList<String> players = new ArrayList<>();
    private Button pickTeamBtn;
    private TextView dTeamName;
    private TextView teamLabel;
    private TextView startDate;
    private TextView winnerLabel;
    private ImageView dTeamCrest;
    private TextView dWinnerLabel;
    private String mUserID;
    private TextView dWinnerName;
    private boolean admin = false;
    boolean win;
    String title;
    private ArrayList<StandingClass> standingDetails = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_details);

        title= getIntent().getExtras().getString("tournament_name");

        System.out.println("*******************************************************TEST*********************************************************************");
        System.out.println(title);

        tName = (TextView) findViewById(R.id.tournamentName);
        tName.setText(title);

        pickTeamBtn = (Button) findViewById(R.id.pickTeamBtn);
        dTeamName = (TextView) findViewById(R.id.detailsTeamName);
        dTeamCrest = (ImageView) findViewById(R.id.detailsTeamCrest);
        teamLabel = (TextView) findViewById(R.id.teamLabel);
        winnerLabel = (TextView) findViewById(R.id.winner);
        winnerLabel = (TextView) findViewById(R.id.winner);
        startDate = (TextView) findViewById(R.id.startDate);

        pickTeamBtn.setVisibility(View.INVISIBLE);
        dTeamName.setVisibility(View.INVISIBLE);
        dTeamCrest.setVisibility(View.INVISIBLE);
        teamLabel.setVisibility(View.INVISIBLE);
        winnerLabel.setVisibility(View.INVISIBLE);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUserID = user.getUid();

        //attaching listener to button
        pickTeamBtn.setOnClickListener(this);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference currentGameweekRef = database.getReference("Fixtures").child("Current Week");

        currentGameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String currentGameweek = dataSnapshot.getValue().toString();
                DatabaseReference deadlineRef = database.getReference("Fixtures").child(currentGameweek).child("Deadline");

                deadlineRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        startDate.setText(dataSnapshot.getValue().toString());
                        final Date currentTime = new Date();
                        final SimpleDateFormat sdf = new SimpleDateFormat("d MMM, HH:mm");
                        final TimeZone timezone = TimeZone.getTimeZone("Europe/Dublin");
                        timezone.useDaylightTime();
                        sdf.setTimeZone(timezone);
                        final Date deadline;
                        try {
                            deadline = sdf.parse(dataSnapshot.getValue().toString());
                            if((sdf.format(currentTime)).compareTo(sdf.format(deadline)) >= 0) {
                                pickTeamBtn.setVisibility(View.INVISIBLE);
                                winnerLabel.setText("You Missed the Deadline!");
                                winnerLabel.setVisibility(View.VISIBLE);
                            } else {
                                pickTeamBtn.setVisibility(View.VISIBLE);
                            }
                        } catch (ParseException e) {
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference ref = database.getReference("Tournaments").child(title).child("Admin");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String adminID = dataSnapshot.child("Admin ID").getValue().toString();
                if(adminID.equals(mUserID)){
                    admin = true;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference myRef1 = database.getReference("Tournaments").child(title).child("Players");
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("***********************************snap**********************************************8");
                System.out.println(dataSnapshot);
                    if(dataSnapshot.child(mUserID).child("Team").getValue().toString().equals("false")){
                        pickTeamBtn.setVisibility(View.VISIBLE);
                        teamLabel.setVisibility(View.INVISIBLE);
                        dTeamName.setVisibility(View.INVISIBLE);
                        dTeamCrest.setVisibility(View.INVISIBLE);
                    }else{
                        dTeamName.setVisibility(View.VISIBLE);
                        dTeamCrest.setVisibility(View.VISIBLE);
                        teamLabel.setVisibility(View.VISIBLE);
                        pickTeamBtn.setVisibility(View.INVISIBLE);
                        dTeamName.setText(dataSnapshot.child(mUserID).child("Team").getValue().toString());
                        String teamCrest = dataSnapshot.child(mUserID).child("Team").getValue().toString().toLowerCase().replace(" ", "_");
                        int crest = TournamentDetailsActivity.this.getResources().getIdentifier(teamCrest, "drawable", TournamentDetailsActivity.this.getPackageName());
                        dTeamCrest.setImageResource(crest);
                    }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        DatabaseReference myRef = database.getReference("Tournaments").child(title).child("Players");

        // List Views
        mListView = (ListView) findViewById(R.id.standingList);
        final ArrayAdapter<StandingClass> standingAdapter = new StandingAdapter(TournamentDetailsActivity.this, 0, standingDetails);
        mListView.setAdapter(standingAdapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("***********************************snap**********************************************8");
                System.out.println(dataSnapshot);
                standingAdapter.clear();
                standingAdapter.notifyDataSetChanged();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    System.out.println(dataSnapshot);
                    if(snap.child("Standing").getValue().toString().equals("Winner")){
                        win = true;
                    }
                    if(snap.child("Standing").getValue().toString().equals("false")){
                        winnerLabel.setText("You are Knocked Out!");
                        winnerLabel.setVisibility(View.VISIBLE);

                        pickTeamBtn.setHeight(View.INVISIBLE);
                        pickTeamBtn.setVisibility(View.INVISIBLE);
                        teamLabel.setVisibility(View.INVISIBLE);
                        dTeamName.setVisibility(View.INVISIBLE);
                        dTeamCrest.setVisibility(View.INVISIBLE);
                    }
                    System.out.println("*********************************************STANDING************************************************");
                    final String standing = snap.child("Standing").getValue().toString();
                    System.out.println(standing);
                    System.out.println("*********************************************END STANDING************************************************");
                    DatabaseReference ref = database.getReference("Users").child(snap.getKey());
                    ref.addValueEventListener(new ValueEventListener()
                    {
                          @Override
                          public void onDataChange(DataSnapshot dataSnapshot) {
                              System.out.println(dataSnapshot);
                              String playerName = dataSnapshot.child("First Name").getValue().toString() + " " + dataSnapshot.child("Last Name").getValue().toString();
                              System.out.println(playerName);
                              standingAdapter.add(new StandingClass(playerName,standing));
                              standingAdapter.notifyDataSetChanged();
                          }

                          @Override
                          public void onCancelled(DatabaseError databaseError) {

                          }
                    });
                }
                if(win){
                    winnerLabel.setVisibility(View.VISIBLE);
                    pickTeamBtn.setHeight(View.INVISIBLE);
                    pickTeamBtn.setVisibility(View.INVISIBLE);
                    teamLabel.setVisibility(View.INVISIBLE);
                    dTeamName.setVisibility(View.INVISIBLE);
                    dTeamCrest.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tournament_deatail_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_settings) {
            if(admin) {
                Intent i = new Intent(this, TournamentAdminActivity.class);
                i.putExtra("tournament_name", title);
                this.startActivity(i);
            }
            else{
                Intent intent = new Intent(this, TournamentPlayerActivity.class);
                intent.putExtra("tournament_name", title);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        //calling register method on click
        Intent intent = new Intent(this, PickTeamActivity.class);
        intent.putExtra("tournament_name", tName.getText());
        startActivity(intent);
    }

}
