package com.ian.laststanding;

public class PlayerClass {
    String playerName;
    String uID;
    String tournament;

    public PlayerClass(String playerName, String uID, String tournament){
        this.playerName = playerName;
        this.uID = uID;
        this.tournament = tournament;
    }

    public String getPlayerName(){
        return playerName;
    }

    public String getuID() {
        return uID;
    }

    public String getTournament() { return tournament; }
}
