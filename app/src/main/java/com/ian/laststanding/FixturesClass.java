package com.ian.laststanding;

public class FixturesClass {
    String homeCrest;
    String homeTeam;
    String homeGoals;
    String awayCrest;
    String awayTeam;
    String awayGoals;

    public FixturesClass(String homeCrest, String homeTeam, String homeGoals, String awayCrest, String awayTeam, String awayGoals){
        this.homeCrest = homeCrest;
        this.homeTeam = homeTeam;
        this.homeGoals = homeGoals;
        this.awayCrest = awayCrest;
        this.awayTeam = awayTeam;
        this.awayGoals = awayGoals;
    }

    public String getHomeCrest(){
        return homeCrest;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getHomeGoals() {
        return homeGoals;
    }

    public String getAwayCrest(){
        return awayCrest;
    }

    public String getAwayTeam(){
        return awayTeam;
    }

    public String getAwayGoals() {
        return awayGoals;
    }
}
