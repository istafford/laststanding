package com.ian.laststanding;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TeamAdapter extends ArrayAdapter<TeamClass>  {
    private Context context;
    private List<TeamClass> teamDetails;
    private String team;

    //constructor, call on creation
    public TeamAdapter(Context context, int resource, ArrayList<TeamClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.teamDetails = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {
        //get the property we are displaying
        TeamClass teams = teamDetails.get(position);
        team = teams.teamName;

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.team_list_layout, null);

        ImageView teamCrest = (ImageView) view.findViewById(R.id.teamCrest);
        TextView teamName = (TextView) view.findViewById(R.id.team);
        //set price and rental attributes
        teamName.setText(teams.teamName);

        //get the image associated with this property
        int crest = context.getResources().getIdentifier(teams.getTeamCrest(), "drawable", context.getPackageName());
        teamCrest.setImageResource(crest);


        return view;
    }
}
