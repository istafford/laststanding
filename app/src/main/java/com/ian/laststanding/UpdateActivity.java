package com.ian.laststanding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText homeGoals;
    private EditText awayGoals;
    private EditText weekNum;
    private Button showBtn;
    private TextView matchNum;
    private TextView homeTeam;
    private TextView awayTeam;
    private String homeResult;
    private String awayResult;
    private int game = 1;
    private int standing = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        matchNum = (TextView) findViewById(R.id.matchNum);
        homeTeam = (TextView) findViewById(R.id.homeTeam);
        awayTeam = (TextView) findViewById(R.id.awayTeam);
        weekNum = (EditText) findViewById(R.id.weekNum);
        homeGoals = (EditText) findViewById(R.id.homeGoals);
        awayGoals = (EditText) findViewById(R.id.awayGoals);

        showBtn = (Button) findViewById(R.id.showBtn);

        showBtn.setOnClickListener(this);
    }

    public void show(){
        final String weekNumber = weekNum.getText().toString().trim();
        final String gameNumber = Integer.toString(game);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference gameweekRef1 = database.getReference("Fixtures");
        gameweekRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gameweekRef1.child("Current Week").setValue(weekNumber);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        matchNum.setText(gameNumber);
        DatabaseReference gameweekRef = database.getReference("Fixtures").child(weekNumber).child("Matches").child(gameNumber);
        gameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                homeTeam.setText(dataSnapshot.child("Home").getValue().toString());
                homeGoals.setText(dataSnapshot.child("Home Goals").getValue().toString());
                awayTeam.setText(dataSnapshot.child("Away").getValue().toString());
                awayGoals.setText(dataSnapshot.child("Away Goals").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void nextMatch(View v){
        game++;
        String weekNumber = weekNum.getText().toString().trim();
        final String gameNumber = Integer.toString(game);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        matchNum.setText(gameNumber);
        DatabaseReference gameweekRef = database.getReference("Fixtures").child(weekNumber).child("Matches").child(gameNumber);
        gameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                homeTeam.setText(dataSnapshot.child("Home").getValue().toString());
                homeGoals.setText(dataSnapshot.child("Home Goals").getValue().toString());
                awayTeam.setText(dataSnapshot.child("Away").getValue().toString());
                awayGoals.setText(dataSnapshot.child("Away Goals").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void updateScore(View v){
        final String homeScore = homeGoals.getText().toString().trim();
        final String awayScore = awayGoals.getText().toString().trim();
        final int homeS = Integer.parseInt(homeScore);
        final int awayS = Integer.parseInt(awayScore);
        final String weekNumber = weekNum.getText().toString().trim();
        final String gameNumber = Integer.toString(game);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        matchNum.setText(gameNumber);
        final DatabaseReference gameweekRef = database.getReference("Fixtures");
        gameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gameweekRef.child(weekNumber).child("Matches").child(gameNumber).child("Home Goals").setValue(homeScore);
                gameweekRef.child(weekNumber).child("Matches").child(gameNumber).child("Away Goals").setValue(awayScore);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        if(homeS > awayS){
            final DatabaseReference ref = database.getReference("Fixtures");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ref.child(weekNumber).child("Results").child(homeTeam.getText().toString()).setValue("Win");
                    homeResult = "Win";
                    ref.child(weekNumber).child("Results").child(awayTeam.getText().toString()).setValue("Loss");
                    awayResult = "Loss";
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        else if (homeS<awayS){
            final DatabaseReference ref = database.getReference("Fixtures");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ref.child(weekNumber).child("Results").child(homeTeam.getText().toString()).setValue("Loss");
                    homeResult = "Loss";
                    ref.child(weekNumber).child("Results").child(awayTeam.getText().toString()).setValue("Win");
                    awayResult = "Win";
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        else {
            final DatabaseReference ref = database.getReference("Fixtures");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ref.child(weekNumber).child("Results").child(homeTeam.getText().toString()).setValue("Draw");
                    homeResult = "Draw";
                    ref.child(weekNumber).child("Results").child(awayTeam.getText().toString()).setValue("Draw");
                    awayResult = "Draw";
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        updateStandings();
    }

    public void updateStandings(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref1 = database.getReference("Tournaments");
        ref1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    final String tName = snap.getKey();
                    final DatabaseReference ref2 = database.getReference("Tournaments").child(tName).child("Players");
                    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                final String uID = snap.getKey();
                                final DatabaseReference ref3 = database.getReference("Tournaments").child(tName).child("Players").child(uID);
                                ref3.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.child("Team").getValue().toString().equals(homeTeam.getText().toString().trim()) && homeResult.equals("Win")){
                                                ref3.child("Standing").setValue(true);
                                                ref3.child("Team").setValue(false);
                                                standing ++;
                                            }
                                            else if(dataSnapshot.child("Team").getValue().toString().equals(homeTeam.getText().toString().trim()) && homeResult.equals("Draw")){
                                                ref3.child("Standing").setValue(true);
                                                ref3.child("Team").setValue(false);
                                                standing ++;
                                            }
                                            else if(dataSnapshot.child("Team").getValue().toString().equals(homeTeam.getText().toString().trim()) && homeResult.equals("Loss")){
                                                ref3.child("Standing").setValue(false);
                                                ref3.child("Team").setValue(false);
                                                standing --;
                                            }
                                            else if(dataSnapshot.child("Team").getValue().toString().equals(awayTeam.getText().toString().trim()) && awayResult.equals("Win")){
                                                ref3.child("Standing").setValue(true);
                                                ref3.child("Team").setValue(false);
                                                standing ++;
                                            }
                                            else if(dataSnapshot.child("Team").getValue().toString().equals(awayTeam.getText().toString().trim()) && awayResult.equals("Draw")){
                                                ref3.child("Standing").setValue(true);
                                                ref3.child("Team").setValue(false);
                                                standing ++;
                                            }
                                            else if(dataSnapshot.child("Team").getValue().toString().equals(awayTeam.getText().toString().trim()) && awayResult.equals("Loss")){
                                                ref3.child("Standing").setValue(false);
                                                ref3.child("Team").setValue(false);
                                                standing --;
                                            }
                                            else{
                                                ref3.child("Standing").setValue(false);
                                                ref3.child("Team").setValue(false);
                                                standing --;
                                            }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });/**/
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void previousMatch(View v){
        game--;
        String weekNumber = weekNum.getText().toString().trim();
        final String gameNumber = Integer.toString(game);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        matchNum.setText(gameNumber);
        DatabaseReference gameweekRef = database.getReference("Fixtures").child(weekNumber).child("Matches").child(gameNumber);
        gameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                homeTeam.setText(dataSnapshot.child("Home").getValue().toString());
                homeGoals.setText(dataSnapshot.child("Home Goals").getValue().toString());
                awayTeam.setText(dataSnapshot.child("Away").getValue().toString());
                awayGoals.setText(dataSnapshot.child("Away Goals").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        show();
    }
}
