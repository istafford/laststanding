package com.ian.laststanding;

public class TournamentClass {

     String tName;
     String aName;

    //default constructor
    public TournamentClass(){
        this.tName = "";
        this.aName = "";
    }

    public TournamentClass(String tName){
        this.tName = tName;
    }

    public TournamentClass(String tName, String aName){
        this.tName = tName;
        this.aName = aName;
    }

    public String getTname(){
        return tName;
    }

    public String getAName(){
        return aName;
    }
}
