package com.ian.laststanding;

public class TeamClass {
    String teamName;
    String teamCrest;

    public TeamClass(String teamName, String teamCrest){
        this.teamName = teamName;
        this.teamCrest = teamCrest;
    }

    public String getTeamName(){
        return teamName;
    }

    public String getTeamCrest() {
        return teamCrest;
    }
}
