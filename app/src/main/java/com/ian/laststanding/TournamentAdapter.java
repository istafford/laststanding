package com.ian.laststanding;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TournamentAdapter extends ArrayAdapter<TournamentClass> {
    private Context context;
    private List<TournamentClass> tournamentDetails;

    //constructor, call on creation
    public TournamentAdapter(Context context, int resource, ArrayList<TournamentClass> objects) {
        super(context, resource, objects);

        this.context = context;
        this.tournamentDetails = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {

        //get the property we are displaying
        TournamentClass tournaments = tournamentDetails.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.tournament_list_layout, null);

        TextView tournamentName = (TextView) view.findViewById(R.id.tournament_name);
        TextView adminName = (TextView) view.findViewById(R.id.admin_name);

        //set tournament and admin
        tournamentName.setText(tournaments.tName);
        adminName.setText(tournaments.aName);

        return view;
    }
}