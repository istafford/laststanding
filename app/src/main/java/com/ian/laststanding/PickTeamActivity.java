package com.ian.laststanding;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.R.id.list;

public class PickTeamActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<TeamClass> teamDetails = new ArrayList<>();

    private final FirebaseDatabase database = FirebaseDatabase.getInstance();

    private String mUser_ID;
    private ListView mListView;
    private String teamName;
    private Button pickBtn;
    private String tName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_team);

        pickBtn = (Button) findViewById(R.id.pickBtn);
        pickBtn.setOnClickListener(this);
        tName = getIntent().getExtras().getString("tournament_name");
        System.out.println("****************************************************TITLE*******************************************************************");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mUser_ID = user.getUid();
        System.out.println("****************************************************USER_ID*******************************************************************");
        System.out.println(mUser_ID);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("Tournaments").child(tName).child("Players").child(mUser_ID).child("Available Teams");

        // List Views
        mListView = (ListView) findViewById(R.id.teamList);
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mListView.setSelector(R.color.secondary_color);
        final ArrayAdapter<TeamClass> teamAdapter = new TeamAdapter(PickTeamActivity.this, 0, teamDetails);
        mListView.setAdapter(teamAdapter);


        myRef.addValueEventListener(new ValueEventListener() {
            int count = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("*****************************************************************TEST 1********************************************************************************************************");
                System.out.println(dataSnapshot.getValue());
                teamAdapter.clear();
                teamAdapter.notifyDataSetChanged();
               for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    System.out.println("*************************************************************************************************************************************************************************");
                    System.out.println(snap.getValue());
                   if(snap.getValue().equals(false)){
                       count ++;
                       if(count == 20){
                           DatabaseReference newTournInfo = database.getReference("Tournaments");
                           FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                           String uID = user.getUid();
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Arsenal").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Bournemouth").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Burnley").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Chelsea").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Crystal Palace").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Everton").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Hull City").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Leicester").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Liverpool").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Man City").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Man Utd").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Middlesbrough").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Southampton").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Stoke").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Sunderland").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Swansea").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Tottenham").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("Watford").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("West Brom").setValue(true);
                           newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child("West Ham").setValue(true);
                       }
                   }
                   if(snap.getValue().equals(true)){
                       String teamName = snap.getKey();
                       String teamCrest = snap.getKey().toLowerCase().replace(" ", "_");
                       teamAdapter.add(new TeamClass(teamName, teamCrest));
                       teamAdapter.notifyDataSetChanged();
                   }
                }
                System.out.println(count);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                TeamClass team = (TeamClass) mListView.getItemAtPosition(position);
                teamName = team.getTeamName();
            }
        });
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Pick Team")
                .setMessage("Are you sure you want to pick this team?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseReference newTournInfo = database.getReference("Tournaments");
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        String uID = user.getUid();
                        newTournInfo.child(tName).child("Players").child(uID).child("Team").setValue(teamName);
                        newTournInfo.child(tName).child("Players").child(uID).child("Available Teams").child(teamName).setValue(false);
                        Toast.makeText(getApplicationContext(), "You Picked " + teamName+"!", Toast.LENGTH_SHORT).show();
                        open();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_input_add)
                .show();
    }

    public void open(){
        Intent intent = new Intent(this, TournamentDetailsActivity.class);
        intent.putExtra("tournament_name", tName);
        startActivity(intent);
    }
}
