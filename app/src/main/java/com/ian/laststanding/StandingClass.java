package com.ian.laststanding;

public class StandingClass {
    String playerName;
    String standing;

    public StandingClass(String playerName, String standing){
        this.playerName = playerName;
        this.standing = standing;
    }

    public String getPlayerName(){
        return playerName;
    }

    public String getStanding() {
        return standing;
    }
}
