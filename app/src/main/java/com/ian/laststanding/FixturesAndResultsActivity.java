package com.ian.laststanding;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FixturesAndResultsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;

    private ArrayList<FixturesClass> fixturesProperties = new ArrayList<>();

    private TextView mUser_name;
    private TextView mUser_email;
    private String mGameweek;
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();

    private TextView mGameweek_Field;
    private TextView mDeadline_Field;
    private ListView mListView;
    private ProgressBar mProgressBar;
    private Button mPreviousBtn;
    private Button mNextBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixtures_and_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        mPreviousBtn = (Button) findViewById(R.id.prevWeekBtn);
        mPreviousBtn.setEnabled(false);
        mNextBtn = (Button) findViewById(R.id.nextWeekBtn);
        mNextBtn.setEnabled(false);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameweekRef = database.getReference("Fixtures");
        mGameweek_Field = (TextView) findViewById(R.id.weekNo);
        mDeadline_Field = (TextView) findViewById(R.id.roundDeadline);

        gameweekRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("************************************************************TEST 3**************************************************************************");
                System.out.println(dataSnapshot.getValue());
                    System.out.println("************************************************************TEST 4**************************************************************************");
                    System.out.println(dataSnapshot.getKey());
                    mGameweek_Field.setText("Week " + dataSnapshot.child("Current Week").getValue().toString());
                    mDeadline_Field.setText(dataSnapshot.child(dataSnapshot.child("Current Week").getValue().toString()).child("Deadline").getValue().toString());

                    mGameweek = dataSnapshot.child("Current Week").getValue().toString();
                    DatabaseReference myRef = database.getReference("Fixtures").child(mGameweek).child("Matches");
                    // List Views
                    mListView = (ListView) findViewById(R.id.fixtureList);
                    final ArrayAdapter<FixturesClass> fixturesAdapter = new FixturesAdapter(FixturesAndResultsActivity.this, 0, fixturesProperties);
                    mListView.setAdapter(fixturesAdapter);
                    myRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                               System.out.println("************************************************************TEST 1**************************************************************************");
                                System.out.println(dataSnapshot.getKey());
                                System.out.println(dataSnapshot.getValue());
                                fixturesAdapter.clear();
                                fixturesAdapter.notifyDataSetChanged();
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    System.out.println("************************************************************TEST 2**************************************************************************");
                                    System.out.println(child.getKey());
                                    System.out.println(child.getValue());
                                    String homeBadge = child.child("Home").getValue().toString().toLowerCase().replace(" ", "_");
                                    String homeTeam = child.child("Home").getValue().toString();
                                    String homeGoals = child.child("Home Goals").getValue().toString();
                                    String awayGoals = child.child("Away Goals").getValue().toString();
                                    String awayTeam = child.child("Away").getValue().toString();
                                    String awayBadge = child.child("Away").getValue().toString().toLowerCase().replace(" ", "_");
                                    System.out.println("**************************************HCREST***********************************" + homeBadge);
                                    System.out.println("**************************************HName***********************************" + homeTeam);
                                    System.out.println("**************************************HSCORE***********************************" + homeGoals);
                                    System.out.println("**************************************ACREST***********************************" + awayBadge);
                                    System.out.println("**************************************AName***********************************" + awayTeam);
                                    System.out.println("**************************************ASCORE***********************************" + awayGoals);
                                    fixturesAdapter.add(new FixturesClass(homeBadge, homeTeam, homeGoals, awayBadge, awayTeam, awayGoals));
                                    fixturesAdapter.notifyDataSetChanged();
                                }
                                mProgressBar.setVisibility(View.GONE);
                                mPreviousBtn.setEnabled(true);
                                mNextBtn.setEnabled(true);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }

                    });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fixtures_and_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tournament) {
            Intent intent = new Intent(this, TournamentActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_fixtures) {
            onBackPressed();
        } else if (id == R.id.nav_how_to_play) {
            Intent intent = new Intent(this, HowToPlayActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            //Intent intent = new Intent(this, ProfileActivity.class);
            //startActivity(intent);
        } else if (id == R.id.nav_help) {
            // Intent intent = new Intent(this, HelpActivity.class);
            //startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void previousWeek (View v) {
        int x = Integer.parseInt(mGameweek);
        if(x != 1) {
            x--;
            mGameweek = Integer.toString(x);
            mGameweek_Field.setText("Week " + mGameweek);

            DatabaseReference matchesRef = database.getReference("Fixtures").child(mGameweek).child("Matches");
            DatabaseReference deadlineRef = database.getReference("Fixtures").child(mGameweek).child("Deadline");

            deadlineRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mDeadline_Field.setText(dataSnapshot.getValue().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            // List Views
            mListView = (ListView) findViewById(R.id.fixtureList);
            final ArrayAdapter<FixturesClass> previousFixturesAdapter = new FixturesAdapter(this, 0, fixturesProperties);
            mListView.setAdapter(previousFixturesAdapter);
            matchesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    System.out.println("************************************************************TEST 1**************************************************************************");
                    System.out.println(dataSnapshot.getKey());
                    System.out.println(dataSnapshot.getValue());
                    previousFixturesAdapter.clear();
                    previousFixturesAdapter.notifyDataSetChanged();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        System.out.println("************************************************************TEST 2**************************************************************************");
                        System.out.println(child.getKey());
                        System.out.println(child.getValue());
                        String homeBadge = child.child("Home").getValue().toString().toLowerCase().replace(" ", "_");
                        String homeTeam = child.child("Home").getValue().toString();
                        String homeGoals = child.child("Home Goals").getValue().toString();
                        String awayGoals = child.child("Away Goals").getValue().toString();
                        String awayTeam = child.child("Away").getValue().toString();
                        String awayBadge = child.child("Away").getValue().toString().toLowerCase().replace(" ", "_");
                        previousFixturesAdapter.add(new FixturesClass(homeBadge, homeTeam, homeGoals,awayBadge , awayTeam, awayGoals));
                        previousFixturesAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }

            });
        }
    }

    public void nextWeek (View v) {
        int x = Integer.parseInt(mGameweek);
        if(x != 38) {
            x++;
            mGameweek = Integer.toString(x);
            mGameweek_Field.setText("Week " + mGameweek);

            DatabaseReference myRef = database.getReference("Fixtures").child(mGameweek).child("Matches");
            DatabaseReference deadlineRef = database.getReference("Fixtures").child(mGameweek).child("Deadline");

            deadlineRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mDeadline_Field.setText(dataSnapshot.getValue().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            // List Views
            mListView = (ListView) findViewById(R.id.fixtureList);
            final ArrayAdapter<FixturesClass> fixturesAdapter = new FixturesAdapter(this, 0, fixturesProperties);
            mListView.setAdapter(fixturesAdapter);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    System.out.println("************************************************************TEST 1**************************************************************************");
                    System.out.println(dataSnapshot.getKey());
                    System.out.println(dataSnapshot.getValue());
                    fixturesAdapter.clear();
                    fixturesAdapter.notifyDataSetChanged();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        System.out.println("************************************************************TEST 2**************************************************************************");
                        System.out.println(child.getKey());
                        System.out.println(child.getValue());
                        String homeBadge = child.child("Home").getValue().toString().toLowerCase().replace(" ", "_");
                        String homeTeam = child.child("Home").getValue().toString();
                        String homeGoals = child.child("Home Goals").getValue().toString();
                        String awayGoals = child.child("Away Goals").getValue().toString();
                        String awayTeam = child.child("Away").getValue().toString();
                        String awayBadge = child.child("Away").getValue().toString().toLowerCase().replace(" ", "_");
                        fixturesAdapter.add(new FixturesClass(homeBadge, homeTeam, homeGoals,awayBadge , awayTeam, awayGoals));
                        fixturesAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }

            });
        }
    }
}
